package io.vertx.starter;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.Future;
import io.vertx.ext.web.Router;
import io.vertx.ext.mongo.MongoClient;
import io.vertx.ext.web.*;
import io.vertx.core.http.*;
import java.util.HashSet;
import java.util.Set;

import io.vertx.core.Vertx;
import io.vertx.core.eventbus.EventBus;
import io.vertx.core.http.HttpHeaders;
import io.vertx.core.http.HttpMethod;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.core.streams.ReadStream;
import io.vertx.ext.mongo.MongoClient;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.handler.*;
import io.vertx.ext.web.handler.CorsHandler;
import io.vertx.ext.web.handler.sockjs.BridgeOptions;
import io.vertx.ext.web.handler.sockjs.PermittedOptions;
import io.vertx.ext.web.handler.sockjs.SockJSHandler;
import io.vertx.core.eventbus.*;
import io.vertx.ext.mongo.MongoClient;
import io.vertx.codegen.annotations.Fluent;
import io.vertx.codegen.annotations.ProxyGen;
import io.vertx.codegen.annotations.VertxGen;
import io.vertx.core.AsyncResult;
import io.vertx.core.*;
import io.vertx.codegen.annotations.*;
import io.vertx.serviceproxy.ServiceBinder;

class DBServiceImpl implements DBService {

  private final MongoClient mongo;

  DBServiceImpl(MongoClient dbClient, Handler<AsyncResult<DBService>> readyHandler) {
    this.mongo = dbClient;
    readyHandler.handle(Future.succeededFuture(this));
  }

  @Override
  public DBService getMenuTree(Handler<AsyncResult<JsonArray>> resultHandler) {
	  try {
			final JsonArray pipeline = new JsonArray();
			JsonObject groupByQuery = new JsonObject();
			JsonObject group = new JsonObject();
			JsonObject set = new JsonObject();
			JsonObject id = new JsonObject();
			set.put("$push", "$$ROOT");
			group.put("_id", "$parent");
			group.put("set", set);
			groupByQuery.put("$group", group);
			pipeline.add(new JsonObject().put("$sort", new JsonObject().put("_id", 1)));
			pipeline.add(groupByQuery);
			JsonObject command = new JsonObject().put("aggregate", "menus").put("pipeline", pipeline);

			JsonObject countQuery = new JsonObject();
			mongo.count("menus", countQuery, res -> {
				if (res.succeeded()) {
					long num = res.result();
					if (num > 0) {
						ReadStream<JsonObject> readStream = mongo.aggregate("menus", pipeline);
						JsonArray dbRecords = new JsonArray();

						readStream.handler(jsonObject -> {
							dbRecords.add(jsonObject);
						});
						readStream.endHandler(handler -> {
							try {
								
							if(dbRecords.size() > 0) {
								final JsonArray returnList = getRoot(dbRecords);
								if(returnList.size() > 0) {
									int parsedCount = 0;
									int iterationCount = 0;
									for (int i = 0;; i++) {
										iterationCount++;
										if (i >= dbRecords.size()) {
											i = 0;
										}
										JsonObject dataset = dbRecords.getJsonObject(i);
										if (!dataset.containsKey("parsed")) {
											if (dataset.getString("_id") == null) {
												dataset.put("parsed", true);
												parsedCount++;
											} else {
												JsonObject parent = recursive(returnList, dataset.getString("_id"));
												if (!parent.isEmpty()) {
													parent.put("children", dataset.getJsonArray("set"));
													dataset.put("parsed", true);
													parsedCount++;
												}
											}
										}
										if (parsedCount == dbRecords.size()/* || iterationCount > 5000 */) {
											break;
										}
									}
								}
								resultHandler.handle(Future.succeededFuture(returnList));
							}
							}
							catch(Exception err) {
								System.out.println(err);
								resultHandler.handle(Future.failedFuture(err.getMessage()));
							}
						});

						readStream.exceptionHandler(handler -> {
							System.out.println(" Err while reading stream  : " + handler);
							resultHandler.handle(Future.failedFuture(handler.getMessage()));
						});
					} else {
						resultHandler.handle(Future.succeededFuture(new JsonArray()));
					}
				} else {
					res.cause().printStackTrace();
					resultHandler.handle(Future.failedFuture(res.cause()));
				}
			});

		} catch (Exception err) {
			System.out.println(err);
			resultHandler.handle(Future.failedFuture(err.getMessage()));
		}
	  return this;
  }
  
  @Override
  public DBService addMenuNode(JsonObject menu, Handler<AsyncResult<JsonArray>> resultHandler) {
	  mongo.insert("menus", menu, lookup -> { 
		  if (lookup.failed()) {
			  resultHandler.handle(Future.failedFuture(lookup.cause())); 
		  } 
		  String id = lookup.result(); 
		  JsonObject findQuery = new JsonObject().put("_id", id);
		  mongo.findOne("menus", findQuery, new JsonObject(), findResult -> {
			  if(findResult.failed()) {
				  resultHandler.handle(Future.failedFuture(findResult.cause())); 
			  }
			  resultHandler.handle(Future.succeededFuture(new JsonArray().add(findResult.result()))); 
		  }); 
	  });
		 
	  return this;
  }
  
  @Override
  public DBService deleteMenuNode(JsonObject deleteQuery, Handler<AsyncResult<JsonObject>> resultHandler){
	  mongo.removeOne("menus", deleteQuery, delResult -> {
		  if(delResult.failed()) {
			  resultHandler.handle(Future.failedFuture(delResult.cause())); 
		  }
		  resultHandler.handle(Future.succeededFuture(new JsonObject().put("success", true))); 
	  });
	  
	  return this;
  }
  
  @Override
  public DBService saveEditorData(JsonObject msg, Handler<AsyncResult<JsonObject>> resultHandler) {
	  JsonObject query = new JsonObject();
	  mongo.find("text", query, res -> {
		  if (res.succeeded()) {
			  if (!res.result().isEmpty()) { // At least one text record found.
				  JsonObject txtRecord = res.result().get(0);
				  String newStr = txtRecord.getString("userInput") + msg.getString("message");

				  JsonObject innerQuery = new JsonObject().put("_id", txtRecord.getString("_id"));
				  // Set the userInput field
				  JsonObject update = new JsonObject().put("$set", new JsonObject().put("userInput", newStr));
				  mongo.updateCollection("text", innerQuery, update, updateRes -> {
					  if (updateRes.succeeded()) {
						  mongo.findOne("text", innerQuery, new JsonObject(), findResult -> {
							  if(findResult.failed()) {
								  resultHandler.handle(Future.failedFuture(findResult.cause())); 
							  }
							  resultHandler.handle(Future.succeededFuture(findResult.result())); 
						  });
					  } else {
						  resultHandler.handle(Future.failedFuture(updateRes.cause())); 
					  }
				  });
			  } else { // No Text record found till now.
				  JsonObject txtRecord = new JsonObject().put("userInput", msg.getString("message"));

				  mongo.insert("text", txtRecord, lookup -> {
					  // error handling
					  if (lookup.failed()) {
						  resultHandler.handle(Future.failedFuture(lookup.cause())); 
					  }

					  String id = lookup.result();
					  JsonObject findQuery = new JsonObject().put("_id", id);
					  mongo.findOne("text", findQuery, new JsonObject(), findResult -> {
						  if(findResult.failed()) {
							  resultHandler.handle(Future.failedFuture(findResult.cause())); 
						  }
						  resultHandler.handle(Future.succeededFuture(findResult.result())); 
					  });

				  });
			  }
		  } else {
			  resultHandler.handle(Future.failedFuture(res.cause())); 
		  }
	  });
		
		return this;
  }
  
  
  private JsonObject recursive(JsonArray parentList, String parentId)
	{ 	
		JsonObject parent = new JsonObject(); 
		for(int j=0; j<parentList.size(); j++){
			if(parentList.getJsonObject(j).getString("_id").equalsIgnoreCase(parentId)){
				parent = parentList.getJsonObject(j); 
				break;
			} else if(parentList.getJsonObject(j).containsKey("children"))
			{ 
				parent = recursive(parentList.getJsonObject(j).getJsonArray("children"), parentId); 
				if(parent != null && !parent.isEmpty()) {
					break;
				}
			}
		} 
		return parent; 
	}
	
	private JsonArray getRoot(JsonArray dbRecords) {
		for (int i = 0;i<dbRecords.size(); i++) {
			JsonObject dataset = dbRecords.getJsonObject(i);
			if (dataset.getString("_id") == null) {
				return dataset.getJsonArray("set");
			} 
		}
		return new JsonArray();
	}
}