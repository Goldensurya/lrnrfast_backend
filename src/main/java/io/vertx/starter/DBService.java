package io.vertx.starter;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.Future;
import io.vertx.ext.web.Router;
import io.vertx.ext.mongo.MongoClient;
import io.vertx.ext.web.*;
import io.vertx.core.http.*;
import java.util.HashSet;
import java.util.Set;

import io.vertx.core.Vertx;
import io.vertx.core.eventbus.EventBus;
import io.vertx.core.http.HttpHeaders;
import io.vertx.core.http.HttpMethod;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.core.streams.ReadStream;
import io.vertx.ext.mongo.MongoClient;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.handler.*;
import io.vertx.ext.web.handler.CorsHandler;
import io.vertx.ext.web.handler.sockjs.BridgeOptions;
import io.vertx.ext.web.handler.sockjs.PermittedOptions;
import io.vertx.ext.web.handler.sockjs.SockJSHandler;
import io.vertx.core.eventbus.*;
import io.vertx.ext.mongo.MongoClient;
import io.vertx.codegen.annotations.Fluent;
import io.vertx.codegen.annotations.ProxyGen;
import io.vertx.codegen.annotations.VertxGen;
import io.vertx.core.AsyncResult;
import io.vertx.core.*;
import io.vertx.codegen.annotations.*;
import io.vertx.serviceproxy.ServiceBinder;

@ProxyGen
@VertxGen
public interface DBService {

  @Fluent
  DBService getMenuTree(Handler<AsyncResult<JsonArray>> resultHandler);
  
  @Fluent
  DBService addMenuNode(JsonObject menu, Handler<AsyncResult<JsonArray>> resultHandler);
  
  @Fluent
  public DBService deleteMenuNode(JsonObject deleteQuery, Handler<AsyncResult<JsonObject>> resultHandler);
  
  @Fluent
  public DBService saveEditorData(JsonObject message, Handler<AsyncResult<JsonObject>> resultHandler);
  
  @GenIgnore
  static DBService create(MongoClient dbClient, Handler<AsyncResult<DBService>> readyHandler) {
    return new DBServiceImpl(dbClient, readyHandler);
  }
  
  @GenIgnore
  static DBService createProxy(Vertx vertx, String address) {
    return new DBServiceVertxEBProxy(vertx, address);
  }

}