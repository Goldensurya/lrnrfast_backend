package io.vertx.starter;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.Future;
import io.vertx.ext.web.Router;
import io.vertx.ext.mongo.MongoClient;
import io.vertx.ext.web.*;
import io.vertx.core.http.*;
import java.util.HashSet;
import java.util.Set;

import io.vertx.core.Vertx;
import io.vertx.core.eventbus.EventBus;
import io.vertx.core.http.HttpHeaders;
import io.vertx.core.http.HttpMethod;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.core.streams.ReadStream;
import io.vertx.ext.mongo.MongoClient;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.handler.*;
import io.vertx.ext.web.handler.CorsHandler;
import io.vertx.ext.web.handler.sockjs.BridgeOptions;
import io.vertx.ext.web.handler.sockjs.PermittedOptions;
import io.vertx.ext.web.handler.sockjs.SockJSHandler;
import io.vertx.core.eventbus.*;

public class MainVerticle extends AbstractVerticle {

	private DBService dbService;

	@Override
	public void start(Future<Void> startFuture) {
		Future<Void> steps = startHttpServer();
		dbService = DBService.createProxy(vertx, "db.proxy");
		steps.setHandler(startFuture.completer());
	}

	private Future<Void> startHttpServer() {
		Future<Void> future1 = Future.future();
		Future<String> future = Future.future();
		vertx.deployVerticle(new DBVerticle(), future); 
		
		HttpServer server = vertx.createHttpServer();
		
		Set<String> allowedHeaders = new HashSet<>();
	    allowedHeaders.add("x-requested-with");
	    allowedHeaders.add("Access-Control-Allow-Origin");
	    allowedHeaders.add("origin");
	    allowedHeaders.add("Content-Type");
	    allowedHeaders.add("accept");
	    allowedHeaders.add("X-PINGARUNER");

	    Set<HttpMethod> allowedMethods = new HashSet<>();
	    allowedMethods.add(HttpMethod.GET);
	    allowedMethods.add(HttpMethod.POST);
	    allowedMethods.add(HttpMethod.OPTIONS);
	    /*
	     * these methods aren't necessary for this sample, 
	     * but you may need them for your projects
	     */
	    allowedMethods.add(HttpMethod.DELETE);
	    allowedMethods.add(HttpMethod.PATCH);
	    allowedMethods.add(HttpMethod.PUT);

	    Router router = Router.router(vertx);
	    
		router.route().handler(CorsHandler.create("http://localhost:3000").allowedHeaders(allowedHeaders).allowedMethods(allowedMethods).allowCredentials(true));
		
		
		// Allow events for the designated addresses in/out of the event bus bridge
		BridgeOptions opts = new BridgeOptions().addInboundPermitted(new PermittedOptions().setAddress("editorTextIn"))
				.addOutboundPermitted(new PermittedOptions().setAddress("editorTextOut"));

		// Create the event bus bridge and add it to the router.
		SockJSHandler ebHandler = SockJSHandler.create(vertx).bridge(opts);
		router.route("/eventbus/*").handler(ebHandler);
		
		router.get("/menu").handler(this::getMenu);
		router.post("/menu").handler(this::addMenuNode);
		router.delete("/menu/:id").handler(this::deleteMenuNode);

		server.requestHandler(router).listen(8080, ar -> {
			if (ar.succeeded()) {
				System.out.println("HTTP server running on port 8080");
				future1.complete();
			} else {
				System.err.println("Could not start a HTTP server" + ar.cause());
				future1.fail(ar.cause());
			}
		});

		return future1;
	}

	private void getMenu(RoutingContext context) {
		dbService.getMenuTree(reply -> {
		    if (reply.succeeded()) {
		          context.response().putHeader(HttpHeaders.CONTENT_TYPE, "application/json");
		          context.response().end(reply.result().encode());
		    } else {
		      context.fail(reply.cause());
		    }
		});
	}
	
	private void addMenuNode(RoutingContext context) {
		context.request().bodyHandler(buffer -> {
			JsonObject menu = buffer.toJsonObject();
			dbService.addMenuNode(menu, reply -> {
			    if (reply.succeeded()) {
			          context.response().putHeader(HttpHeaders.CONTENT_TYPE, "application/json");
			          context.response().setStatusCode(201);
			          context.response().end(reply.result().encode());
			    } else {
			      context.fail(reply.cause());
			    }
			});
		});
	}
	
	private void deleteMenuNode(RoutingContext context) {
		String id = context.request().getParam("id");
		JsonObject deleteQuery = new JsonObject().put("_id", id);
		
		dbService.deleteMenuNode(deleteQuery, reply -> {
		    if (reply.succeeded()) {
		          context.response().putHeader(HttpHeaders.CONTENT_TYPE, "application/json");
		          context.response().setStatusCode(202);
		          context.response().end(reply.result().encode());
		    } else {
		      context.fail(reply.cause());
		    }
		});
		
	}
	
}
