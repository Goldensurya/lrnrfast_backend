package io.vertx.starter;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.Future;
import io.vertx.ext.web.Router;
import io.vertx.ext.mongo.MongoClient;
import io.vertx.ext.web.*;
import io.vertx.core.http.*;
import java.util.HashSet;
import java.util.Set;

import io.vertx.core.Vertx;
import io.vertx.core.eventbus.EventBus;
import io.vertx.core.http.HttpHeaders;
import io.vertx.core.http.HttpMethod;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.core.streams.ReadStream;
import io.vertx.ext.mongo.MongoClient;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.handler.*;
import io.vertx.ext.web.handler.CorsHandler;
import io.vertx.ext.web.handler.sockjs.BridgeOptions;
import io.vertx.ext.web.handler.sockjs.PermittedOptions;
import io.vertx.ext.web.handler.sockjs.SockJSHandler;
import io.vertx.core.eventbus.*;
import io.vertx.serviceproxy.ServiceBinder;

public class DBVerticle extends AbstractVerticle {

	private MongoClient mongo;
	private DBService dbService;
	private EventBus eb;
	
	@Override
	public void start(Future<Void> future) {
		mongo = MongoClient.createShared(vertx, new JsonObject().put("db_name", "demo"));
		eb = vertx.eventBus();
		eb.consumer("editorTextIn", this::onMessage);
		
		DBService.create(mongo, ready -> {
			if (ready.succeeded()) {
				ServiceBinder binder = new ServiceBinder(vertx);
				binder
				.setAddress("db.proxy")
				.register(DBService.class, ready.result());
				dbService = DBService.createProxy(vertx, "db.proxy");
				future.complete();
			} else {
				future.fail(ready.cause());
			}
		});
		
	}
	
	public void onMessage(Message<JsonObject> message) {
		JsonObject msg = new JsonObject(message.body().toString());
		dbService.saveEditorData(msg, reply -> {
		    if (reply.succeeded()) {
		          eb.publish("editorTextOut",reply.result().encode());
		    } else {
		    	eb.publish("editorTextOut", " error : " + reply.cause());
		    }
		});
	}

}
